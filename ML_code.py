from lightgbm import LGBMClassifier
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import warnings
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import ppscore as pps
from xgboost import XGBClassifier

warnings.filterwarnings("ignore")

file_name = "./750_image.xlsx"
df = pd.read_excel(io=file_name)
df_n = df[['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10', 'W11', 'W12', 'W13', 'W14', 'W15', 'W16',
           'pose_number']].copy()
print(df_n.size)

# plt.figure(figsize=(16, 12))
# sns.heatmap(pps.matrix(df_n), annot=True, fmt=".2f")
# plt.show()

X_train, X_test, y_train, y_test = train_test_split(df_n.drop('pose_number', axis=1),
                                                    df_n['pose_number'], test_size=0.25)

XGB = XGBClassifier(max_depth=4, learning_rate=0.005, n_estimators=500, n_jobs=-1, min_child_weight=2)
XGB.fit(X_train, y_train)

RFC = RandomForestClassifier(n_estimators=500, max_depth=9, min_samples_split=3)
RFC.fit(X_train, y_train)

lgb = LGBMClassifier(learning_rate=0.005, max_depth=20, n_estimators=500, num_leaves=10).fit(X_train, y_train)

print("Training and testing the results on all datasets")
print("XGB train score:             ", round(XGB.score(X_train, y_train), 4), "   XGB test score:           ",
      round(XGB.score(X_test, y_test), 4))
print("Random Forest's train score: ", round(RFC.score(X_train, y_train), 4), "   Random Forest test score: ",
      round(RFC.score(X_test, y_test), 4))
print("LGBM train score:            ", round(lgb.score(X_train, y_train), 4), "   LGB Model test score:     ",
      round(lgb.score(X_test, y_test), 4))
print("\n")

# Checking on new data

file_name = "./316_image.xlsx"
df = pd.read_excel(io=file_name)
df_n = df[['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10', 'W11', 'W12', 'W13', 'W14', 'W15', 'W16',
           'pose_number']].copy()

# plt.figure(figsize=(16, 12))
# sns.heatmap(pps.matrix(df_n), annot=True, fmt=".2f")
# plt.show()

test_x = df_n.drop(columns=['pose_number'], axis= 1)
test_y = df_n['pose_number']

print("Testing the results on 316 image dataset")
print("XGB test score:             ", round(XGB.score(test_x, test_y), 4))
print("Random Forest's test score: ", round(RFC.score(test_x, test_y), 4))
print("LGBM test score:            ", round(lgb.score(test_x, test_y), 4))
print("\n")

# Checking on new data
file_name = "./285_image.xlsx"
df = pd.read_excel(io=file_name)
df_n = df[['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10', 'W11', 'W12', 'W13', 'W14', 'W15', 'W16',
           'pose_number']].copy()

# plt.figure(figsize=(16, 12))
# sns.heatmap(pps.matrix(df_n), annot=True, fmt=".2f")
# plt.show()

test_x = df_n.drop(columns=['pose_number'], axis= 1)
test_y = df_n['pose_number']

print("Testing the results on 285 image dataset")
print("XGB test score:             ", round(XGB.score(test_x, test_y), 4))
print("Random Forest's test score: ", round(RFC.score(test_x, test_y), 4))
print("LGBM test score:            ", round(lgb.score(test_x, test_y), 4))
print("\n")


# Checking on new data
file_name = "./final9.xlsx"
df = pd.read_excel(io=file_name)
df_n = df[['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10', 'W11', 'W12', 'W13', 'W14', 'W15', 'W16',
           'pose_number']].copy()

# plt.figure(figsize=(16, 12))
# sns.heatmap(pps.matrix(df_n), annot=True, fmt=".2f")
# plt.show()

test_x = df_n.drop(columns=['pose_number'], axis= 1)
test_y = df_n['pose_number']

print("Testing the results on 4th image dataset")
print("XGB test score:             ", round(XGB.score(test_x, test_y), 4))
print("Random Forest's test score: ", round(RFC.score(test_x, test_y), 4))
print("LGBM test score:            ", round(lgb.score(test_x, test_y), 4))
print("\n")


# Checking on new data
file_name = "./final9 (1).xlsx"
df = pd.read_excel(io=file_name)
df_n = df[['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10', 'W11', 'W12', 'W13', 'W14', 'W15', 'W16',
           'pose_number']].copy()

# plt.figure(figsize=(16, 12))
# sns.heatmap(pps.matrix(df_n), annot=True, fmt=".2f")
# plt.show()

test_x = df_n.drop(columns=['pose_number'], axis= 1)
test_y = df_n['pose_number']

print("Testing the results on 5th image dataset")
print("XGB test score:             ", round(XGB.score(test_x, test_y), 4))
print("Random Forest's test score: ", round(RFC.score(test_x, test_y), 4))
print("LGBM test score:            ", round(lgb.score(test_x, test_y), 4))
print("\n")

# Checking on new data
file_name = "./final9 (2).xlsx"
df = pd.read_excel(io=file_name)
df_n = df[['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10', 'W11', 'W12', 'W13', 'W14', 'W15', 'W16',
           'pose_number']].copy()

# plt.figure(figsize=(16, 12))
# sns.heatmap(pps.matrix(df_n), annot=True, fmt=".2f")
# plt.show()

test_x = df_n.drop(columns=['pose_number'], axis= 1)
test_y = df_n['pose_number']

print("Testing the results on 6th image dataset")
print("XGB test score:             ", round(XGB.score(test_x, test_y), 4))
print("Random Forest's test score: ", round(RFC.score(test_x, test_y), 4))
print("LGBM test score:            ", round(lgb.score(test_x, test_y), 4))
print("\n")


# Checking on new data
file_name = "./final9 (3).xlsx"
df = pd.read_excel(io=file_name)
df_n = df[['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10', 'W11', 'W12', 'W13', 'W14', 'W15', 'W16',
           'pose_number']].copy()

# plt.figure(figsize=(16, 12))
# sns.heatmap(pps.matrix(df_n), annot=True, fmt=".2f")
# plt.show()

test_x = df_n.drop(columns=['pose_number'], axis= 1)
test_y = df_n['pose_number']

print("Testing the results on 7th image dataset")
print("XGB test score:             ", round(XGB.score(test_x, test_y), 4))
print("Random Forest's test score: ", round(RFC.score(test_x, test_y), 4))
print("LGBM test score:            ", round(lgb.score(test_x, test_y), 4))
print("\n")